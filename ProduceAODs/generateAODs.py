import os,sys,getpass

Samples = [ # samples taken from https://twiki.cern.ch/twiki/bin/viewauth/Atlas/L1CaloUpgradeSimulation
# Dijets
#  'mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.ESD.e3569_s3126_r11881',
#  'mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.recon.ESD.e3569_s3126_r11881',
#  'mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.recon.ESD.e3668_s3126_r11881',
#  'mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.recon.ESD.e3668_s3126_r11881',
#  'mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.recon.ESD.e3668_s3126_r11881',
# HH->4b
  'mc16_13TeV.450000.aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_bbbb.recon.ESD.e7107_e5984_s3126_r11881',
]

###################################
# DO NOT MODIFY (below this line) #
###################################

username = getpass.getuser()

def getMCDSID(sampleName):
  return sampleName[11:17]

command = ''
for sample in Samples:
  dsid = getMCDSID(sample)
  command += 'pathena --inDS '+sample+' --trf "Reco_tf.py --inputESDFile %IN --outputAODFile %OUT.AOD.root --AMIConfig r11328" --outDS user.'+username+'.'+dsid+'.r11328 && '
command = command[:-2]
print(command)
os.system(command)
