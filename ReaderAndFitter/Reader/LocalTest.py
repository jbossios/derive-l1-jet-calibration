# Inputs
InputFiles = [ # path to ROOT files
  "/afs/cern.ch/user/j/jbossios/work/public/JetTrigger/DeriveL1calibration/git/ProduceTTrees/submitDir_361022/data-tree/",
]
Dataset   = "Dijets"

# Debugging
Debug     = False

###################################################
## DO NOT MODIFY
###################################################

from ROOT import *
import os,sys
from Arrays import *

os.system('mkdir Testing')

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)

# Run over a single file from the chosen dataset
counter = 0
command = ""
for File in os.listdir(InputFiles[0]): # Loop over files
  if ".root" not in File:
    continue
  if counter > 0:
    break

  # Run test job
  command += "python Reader.py --path "+InputFiles[0]+" --outPATH Testing/ --file "+File+" --dataset "+Dataset
  if Debug:
    command += " --debug"
  command += " && "
  counter += 1

command = command[:-2]
os.system(command)
