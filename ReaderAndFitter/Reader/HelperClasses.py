from ROOT import TLorentzVector

class iJet(TLorentzVector):
  def __init__(self):
    TLorentzVector.__init__(self)
  def __eq__(self, other):
      return self.Pt() == other.Pt() and self.Eta() == other.Eta() and self.Phi() == other.Phi()
  def __lt__(self, other):
      return self.Pt() < other.Pt()
