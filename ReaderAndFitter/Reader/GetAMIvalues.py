import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

samples = [
  'mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.ESD.e3569_s3126_r11881',
  'mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.recon.ESD.e3569_s3126_r11881',
  'mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.recon.ESD.e3668_s3126_r11881',
  'mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.recon.ESD.e3668_s3126_r11881',
  'mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.recon.ESD.e3668_s3126_r11881',
]


###########################################################################################################
# DO NOT MODIFY (below this line)
###########################################################################################################

Keys = [
  'crossSection',
  'genFiltEff',
  'kFactor@PMG',
]

# Loop over samples
print "DSID \t xs \t eff \t kfactor \n" 
for dataset in samples:
  run = dataset.split(".",2)
  info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  Values = dict()
  for dic in arr:
    d = dic['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)[0]
	for key in Keys:
	  if key in dd.keys():
            if key == 'crossSection':
	      Values[key] = float(dd[key])*1000
            else:
	      Values[key] = float(dd[key])
        break
  for key in Keys:
    if key in Values: info += " " + str(Values[key])
  print info			  
	
