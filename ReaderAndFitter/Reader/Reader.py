###########################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    17/09/2020
## Purpose: Read TTrees and fill histograms
###########################################################################################

# Jet matching and isolation requirements
Rmatch        = 0.3       # matching parameter
RIsoT         = 0.4 * 2.5 # reco-truth isolation requirement
RIsoR         = 0.4 * 1.5 # reco-reco isolation requirement
RecoIsopTCut  = 7         # min pt requirement to discard non-isolated reco jet
TruthIsopTCut = 7         # min pt requirement to discard non-isolated truth jet
      
###########################################################################################
# DO NOT MODIFY (below this line)                                                         #
###########################################################################################

from ROOT import *
import os,sys,argparse
from HelperClasses import *
from HelperFunctions import *
from Arrays import *
from metadata_dict import MetadataFiles
from math import *
#import numpy as np

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--path',            action='store',      dest="path")
parser.add_argument('--outPATH',         action='store',      dest="outPATH")
parser.add_argument('--file',            action='store',      dest="inputFile")
parser.add_argument('--dataset',         action='store',      dest="dataset")
parser.add_argument('--debug',           action='store_true', dest="debug",       default=False)

args = parser.parse_args()

# Sample type
Dataset  = args.dataset

# Debugging
Debug = args.debug

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)
if args.path is None:
  print "ERROR: path to input file not provided, exiting"
  sys.exit(0)
if args.outPATH is None:
  print "ERROR: path to output files (outPATH) not provided, exiting"
  sys.exit(0)
if args.inputFile is None:
  print "ERROR: input file not provided, exiting"
  sys.exit(0)

###############################
# Read AMI info (if requested)
###############################
FileName = "ami_reader.txt"
AMIFile  = open(FileName,'r')
lines    = AMIFile.readlines()
AMIxss = dict() # cross section values
AMIeff = dict() # efficiency values
AMIk   = dict() # k-factor values
for line in lines: # loop over lines
  Line = line.split(" ")
  dsid = int(Line[0]) # first column is dsid
  AMIxss[dsid] = float(Line[1]) # second column is cross section
  AMIeff[dsid] = float(Line[2]) if '\n' not in Line[2] else float(Line[2][:-2]) # third column is efficiency
  if len(Line) > 3: kFactor = Line[3] # fourth column (if available) is k-factor
  else: kFactor = '1.0'
  AMIk[dsid] = float(kFactor) if '\n' not in kFactor else float(kFactor)

#####################
# Output file name
#####################

# Identify DSID/dataPeriod
DSID = getDSID(args.path)

inputFileName  = args.inputFile.replace(".root","")

OutName    = str(args.outPATH)
OutName   += Dataset
OutName   += "_" + DSID
OutName   += "_" + inputFileName
if Debug:
  OutName += "_Debug"
OutName   += ".root"

#####################
# TTree name
#####################
TDirectoryName = "TreeAlgo"

# Max number of events for debuging
DebugMAXevents = 1000

###########################
# Get sumWeights (MC only)
###########################
sumWeights = dict()
# Loop over metadata files
if Dataset not in MetadataFiles:
  print "ERROR: No metadata found for "+Dataset+", exiting"
  sys.exit(0)
for key,value in MetadataFiles[Dataset].items():
  # open metadata file
  MetadataFile = TFile.Open(value)
  if not MetadataFile:
    print "ERROR: "+value+" file not found, exiting"
    sys.exit(0)
  # get histogram
  MetadataHist    = MetadataFile.Get("cutflow")
  sumWeights[key] = MetadataHist.GetBinContent(0)
  MetadataFile.Close()

#############################################
# Open TFile/TDirectory and loop over events
#############################################
print "INFO: Opening "+args.path+args.inputFile
tfile = TFile.Open(args.path+args.inputFile)
if not tfile:
  print "ERROR: "+args.path+args.inputFile+" not found, exiting"
  sys.exit(0)

tdir = TDirectoryFile()
tdir = tfile.Get(TDirectoryName)
if not tdir:
  print "ERROR: "+TDirectoryName+" not found, exiting"
  sys.exit(0)

# Get list of TTrees
Keys = tdir.GetListOfKeys()
keys = []
for key in Keys: keys.append(key.GetName())

# Set list of requested TTrees
keys = ['nominal']

# Loop over TTrees
for TTreeName in keys:

  # Open TFile
  print "INFO: Opening "+args.path+args.inputFile
  tfile = TFile.Open(args.path+args.inputFile)
  if not tfile:
    print "ERROR: "+args.path+args.inputFile+" not found, exiting"
    sys.exit(0)

  # Open corresponding directory
  tdir = TDirectoryFile()
  tdir = tfile.Get(TDirectoryName)
  if not tdir:
    print "ERROR: "+TDirectoryName+" not found, exiting"
    sys.exit(0)

  # Get TTree
  if Debug: print "DEBUG: Get "+TTreeName+" TTree"
  tree = tdir.Get(TTreeName)
  if not tree:
    print "ERROR: "+TTreeName+" not found, exiting"
    sys.exit(0)
  totalEvents = tree.GetEntries()
  print "INFO: TotalEvents in "+TTreeName+": "+str(totalEvents)

  ###################
  # Book histograms
  ###################

  # L1 kinematics
  h_L1pt = TH1D('L1pt','',500,0,500)
  h_L1pt.Sumw2()
  h_L1eta = TH1D('L1eta','',nEtaBins,-4.5,4.5)
  h_L1eta.Sumw2()

  # Response distribution for each eta bin
  h_Response = dict()
  for ibin in xrange(0,nEtaBins): # loop over eta bins
    name = 'Response_vs_pttruth_eta_{}'.format(str(ibin))
    hist = TH2D(name,'',500,0,500,300,0,3) # Response (L1et8x8/pTtruth) vs pttruth for eta bin ibin
    hist.Sumw2()
    h_Response[ibin] = hist

  # L1 et8x8 vs pttruth
  h_ptreco_vs_pttruth = dict()
  for ibin in xrange(0,nEtaBins): # loop over eta bins
    name = "ptreco_vs_pttruth_eta_{}".format(str(ibin))
    hist = TH2D(name,"",500,0,500,500,0,500) # L1 et8x8 (y-axis) vs pttruth (x-axis)
    hist.Sumw2()
    h_ptreco_vs_pttruth[ibin] = hist

  # pttruth vs pttruth (to be able to get <pttruth>)
  h_pttruth_vs_pttruth = dict()
  for ibin in xrange(0,nEtaBins): # loop over eta bins
    name = "pttruth_vs_pttruth_eta_{}".format(str(ibin))
    hist = TH2D(name,"",500,0,500,500,0,500)
    hist.Sumw2()
    h_pttruth_vs_pttruth[ibin] = hist

  ################
  # Set branches
  ################
  Branches = dict()
  setBranchStatus(tree)
  setBranchAddress(tree,Branches)

  ###################
  # Loop over events
  ###################
  for event_counter in xrange(0,totalEvents):

    # Get entry
    tree.GetEntry(event_counter)

    event_counter += 1
    if Debug and event_counter > DebugMAXevents:
      break # skip loop
    if Debug:
      print "DEBUG: EventNumber: "+str(Branches["eventNumber"][0])
    if event_counter == 1:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
    if event_counter % 100000 == 0:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
 
    ####################
    # Get event weights
    ####################
    sampleWeight  = Branches["mcEventWeight"][0]
    sampleWeight *= AMIxss[Branches["mcChannelNumber"][0]]
    sampleWeight *= AMIeff[Branches["mcChannelNumber"][0]]
    sampleWeight *= AMIk[Branches["mcChannelNumber"][0]]
    sampleWeight /= sumWeights[Branches["mcChannelNumber"][0]]
    wgt           = sampleWeight
    if Debug:
      print "DEBUG: mcEventWeight: "+str(Branches["mcEventWeight"][0])
      print "DEBUG: mcChannelNumber: "+str(Branches["mcChannelNumber"][0])
      print "DEBUG: sumWeights: "+str(sumWeights[Branches["mcChannelNumber"][0]])
      print "DEBUG: sampleWeight: "+str(wgt)

    #################
    # Select L1 jets
    #################
    SelectedL1Jets = []
    if Debug: print('Selected L1 jets:')
    for ijet in xrange(0,getNL1Jet(Branches)):
      jetPt  = getL1JetPt(Branches,ijet)
      jetEta = getL1JetEta(Branches,ijet)
      jetPhi = getL1JetPhi(Branches,ijet)
      # protection
      if jetPt < 0: continue # skip negative et8x8 L1 jet
      #if jetPt < 5: continue # skip very low pt L1 jets
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiM(jetPt,jetEta,jetPhi,0) # L1 jets are massless
      if Debug: print('L1 jet pt: '+str(jetPt))
      if Debug: print('L1 jet eta: '+str(jetEta))
      if Debug: print('L1 jet phi: '+str(jetPhi))
      SelectedL1Jets.append(TLVjet)

    ####################
    # Select truth jets
    ####################
    SelectedTruthJets = []
    if Debug: print('Selected Truth jets:')
    for ijet in xrange(0,getNJetTruth(Branches)):
      jetPt  = getJetPtTruth(Branches,ijet)
      jetEta = getJetEtaTruth(Branches,ijet)
      jetPhi = getJetPhiTruth(Branches,ijet)
      jetE   = getJetETruth(Branches,ijet)
      # protection
      if jetE < 0: continue # skip negative energy truth jet
      if jetPt < 7: continue # skip very low pt L1 jets
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiE(jetPt,jetEta,jetPhi,jetE)
      if Debug: print('Truth jet pt: '+str(jetPt))
      if Debug: print('Truth jet Et: '+str(TLVjet.Et()))
      if Debug: print('Truth jet eta: '+str(jetEta))
      if Debug: print('Truth jet phi: '+str(jetPhi))
      SelectedTruthJets.append(TLVjet)

    ###################
    # Event selections
    ###################
    
    # At least one L1 jet
    nL1Jets    = len(SelectedL1Jets)
    if nL1Jets < 1: continue # skip event
    
    # At least one L1 jet
    nTruthJets = len(SelectedTruthJets)
    if nTruthJets < 1: continue # skip event

    # Remove pileup events
    pTavg = SelectedL1Jets[0].Pt();
    if nL1Jets > 1: pTavg = ( SelectedL1Jets[0].Pt() + SelectedL1Jets[1].Pt() ) * 0.5
    if pTavg/SelectedTruthJets[0].Pt() > 1.4: continue # skip event

    #############################################
    # Match jets and fill response distributions
    #############################################

    # Loop over L1 jets
    for lj in xrange(0,nL1Jets):
      ljet = SelectedL1Jets[lj]
      if Debug: print('DEBUG: ptreco   = '+str(ljet.Pt()))

      ######################################
      # Apply L1-truth matching requirement
      ######################################

      # Find the nearest truth jet
      index_1sttruth = -1
      dRmin_1sttruth = 1E6
      dRmin_2ndtruth = 1E6
      dRmin_reco     = 1E6
      # Loop over truth jets
      for tj in xrange(0,nTruthJets):
        tjet = SelectedTruthJets[tj]
        dr   = ljet.DeltaR(tjet)
	if Debug:
          print('DEBUG: truth eta = '+str(tjet.Eta()))
          print('DEBUG: truth phi = '+str(tjet.Phi()))
          print('DEBUG: dr        = '+str(dr))
        if dr < dRmin_1sttruth:
          dRmin_1sttruth = dr
          index_1sttruth = tj
      
      # Skip non-matched L1 jets
      if dRmin_1sttruth > Rmatch:
	if Debug: print('DEBUG: L1Jet({},{}) discarded (not matched to any truth)'.format(ljet.Pt(),ljet.Eta()))
        continue # skip L1 jet

      ###############################
      # Apply isolation requirements
      ###############################
      
      # Find 2nd-nearest truth jet
      for tl in xrange(0,nTruthJets):
        if tl == index_1sttruth: continue
	tjet = SelectedTruthJets[tl]
	if tjet.Pt() < TruthIsopTCut: continue
	dr = ljet.DeltaR(tjet)
	if dr < dRmin_2ndtruth: dRmin_2ndtruth = dr
      if dRmin_2ndtruth < RIsoT: continue # skip L1 jet
      
      # Find nearest L1 jet
      for ll in xrange(0,nL1Jets):
        if lj == ll: continue
	if SelectedL1Jets[ll].Pt < RecoIsopTCut: continue
	dr = ljet.DeltaR(SelectedL1Jets[ll])
	if dr < dRmin_reco: dRmin_reco = dr
      if dRmin_reco < RIsoR: continue # skip L1 jet

      ######################
      # Fill distributions
      ######################

      if Debug: print('DEBUG: Filling distributions')

      h_L1pt.Fill(ljet.Pt(),wgt)
      h_L1eta.Fill(ljet.Eta(),wgt)

      # Identify eta bin
      if   ljet.Eta() < -4.5: ieta = 0
      elif ljet.Eta() >= 4.5: ieta = nEtaBins-1
      else:
        for etabin in range(0,nEtaBins):
          if ljet.Eta() >= EtaBins[etabin] and ljet.Eta() < EtaBins[etabin+1]:
	    ieta = etabin
	    break

      if Debug:
        print('DEBUG: L1 pt  = '+str(ljet.Pt()))
        print('DEBUG: L1 eta = '+str(ljet.Eta()))
        print('DEBUG: ieta   = '+str(ieta))

      # Calculate response
      pttruth  = SelectedTruthJets[index_1sttruth].Pt()
      ptreco   = ljet.Pt() 
      response = ptreco/pttruth
      Eresponse = ljet.E()/SelectedTruthJets[index_1sttruth].E()

      if Debug:
        print('DEBUG: pttruth  = '+str(pttruth))
        print('DEBUG: ptreco   = '+str(ptreco))
        print('DEBUG: response = '+str(response))
        print('DEBUG: Eresponse = '+str(Eresponse))

      # Fill
      h_Response[ieta].Fill(pttruth,response,wgt)
      h_ptreco_vs_pttruth[ieta].Fill(pttruth,ptreco,wgt) 
      h_pttruth_vs_pttruth[ieta].Fill(pttruth,pttruth,wgt)

  ##################################
  # Write histograms to a ROOT file
  ##################################
  outFile = TFile(OutName,"UPDATE")
  h_L1pt.Write()
  h_L1eta.Write()
  for etabin,hist in h_Response.iteritems():
    hist.Write()
  for etabin,hist in h_ptreco_vs_pttruth.iteritems():
    hist.Write()
  for etabin,hist in h_pttruth_vs_pttruth.iteritems():
    hist.Write()
  outFile.Close()
  print "Histograms saved to "+OutName
print ">>> DONE <<<"
