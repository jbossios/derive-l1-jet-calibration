# Metadata files

Dijets         = dict()
Dijets[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361021_170920_cutflow.root/361021_cutflow.root"
Dijets[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361022_170920_cutflow.root/361022_cutflow.root"
Dijets[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361023_170920_cutflow.root/361023_cutflow.root"
Dijets[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361024_170920_cutflow.root/361024_cutflow.root"

DijetsCalibrated         = dict()
DijetsCalibrated[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361021_jFEXcalibrated_220920_cutflow.root/361021_cutflow.root"
DijetsCalibrated[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361022_jFEXcalibrated_220920_cutflow.root/361022_cutflow.root"
DijetsCalibrated[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361023_jFEXcalibrated_220920_cutflow.root/361023_cutflow.root"
DijetsCalibrated[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361024_jFEXcalibrated_220920_cutflow.root/361024_cutflow.root"

MetadataFiles = dict()
MetadataFiles["Dijets"]           = Dijets
MetadataFiles["DijetsCalibrated"] = DijetsCalibrated

