##################################
# Expected datasets
##################################
DatasetOptions = ['Dijets','DijetsCalibrated']

# Eta binning
EtaBins  = [-4.5+x*0.5 for x in xrange(0,19)]
nEtaBins = len(EtaBins)-1
