
#####################
# TRUTH JETS
#####################
def getNJetTruth(Branches):
  return len(Branches["truthJet_pt"])

def getJetPtTruth(Branches,i):
  return Branches["truthJet_pt"][i]

def getJetPhiTruth(Branches,i):
  return Branches["truthJet_phi"][i]

def getJetEtaTruth(Branches,i):
  return Branches["truthJet_eta"][i]

def getJetETruth(Branches,i):
  return Branches["truthJet_E"][i]


#####################
# L1 JETS
#####################
def getNL1Jet(Branches):
  return len(Branches["L1Jet_et8x8"])

def getL1JetPt(Branches,i):
  return Branches["L1Jet_et8x8"][i]

def getL1JetPhi(Branches,i):
  return Branches["L1Jet_phi"][i]

def getL1JetEta(Branches,i):
  return Branches["L1Jet_eta"][i]


#####################
# Other
#####################
def getDSID(path):
  if path.find("3610") != -1:
    return path[path.find("3610"):path.find("3610")+6]
  elif path.find("3129") != -1:
    return path[path.find("3129"):path.find("3129")+6]
  elif path.find("3461") != -1:
    return path[path.find("3461"):path.find("3461")+6]


# SetBranchStatus
def setBranchStatus(tree):
  tree.SetBranchStatus("*",0) # disable all branches
  tree.SetBranchStatus("eventNumber",1)
  tree.SetBranchStatus("mcEventWeight",1)
  tree.SetBranchStatus("mcChannelNumber",1)
  tree.SetBranchStatus("truthJet_pt",1)
  tree.SetBranchStatus("truthJet_phi",1)
  tree.SetBranchStatus("truthJet_eta",1)
  tree.SetBranchStatus("truthJet_E",1)
  tree.SetBranchStatus("L1Jet_et8x8",1)
  tree.SetBranchStatus("L1Jet_eta",1)
  tree.SetBranchStatus("L1Jet_phi",1)


# SetBranchAddress
import ROOT,array
def setBranchAddress(tree, Branches):
  Branches["eventNumber"] = array.array('i',[0])
  tree.SetBranchAddress("eventNumber",Branches["eventNumber"])
  Branches["mcEventWeight"] = array.array('f',[0])
  tree.SetBranchAddress("mcEventWeight",Branches["mcEventWeight"])
  Branches["mcChannelNumber"] = array.array('i',[0])
  tree.SetBranchAddress("mcChannelNumber",Branches["mcChannelNumber"])
  Branches["truthJet_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("truthJet_pt",Branches["truthJet_pt"])
  Branches["truthJet_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("truthJet_eta",Branches["truthJet_eta"])
  Branches["truthJet_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("truthJet_phi",Branches["truthJet_phi"])
  Branches["truthJet_E"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("truthJet_E",Branches["truthJet_E"])
  Branches["L1Jet_et8x8"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("L1Jet_et8x8",Branches["L1Jet_et8x8"])
  Branches["L1Jet_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("L1Jet_eta",Branches["L1Jet_eta"])
  Branches["L1Jet_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("L1Jet_phi",Branches["L1Jet_phi"])
