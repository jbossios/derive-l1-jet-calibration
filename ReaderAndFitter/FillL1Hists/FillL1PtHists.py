#################################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    29/09/2020
## Purpose: Fill L1 jet distributions
#################################################################################################

# Need leading, subleading up to 7th pt distributions for L1 and L1 calibrated
      
###########################################################################################
# DO NOT MODIFY (below this line)                                                         #
###########################################################################################

# SetBranchStatus
def setBranchStatus(tree):
  tree.SetBranchStatus("*",0) # disable all branches
  tree.SetBranchStatus("mcEventWeight",1)
  tree.SetBranchStatus("jFEXcalibrated_et8x8",1)
  tree.SetBranchStatus("jFEXcalibrated_eta",1)
  tree.SetBranchStatus("jFEXcalibrated_phi",1)
  tree.SetBranchStatus("jFEX_et8x8",1)
  tree.SetBranchStatus("jFEX_eta",1)
  tree.SetBranchStatus("jFEX_phi",1)

# SetBranchAddress
import ROOT,array
def setBranchAddress(tree, Branches):
  Branches["mcEventWeight"] = array.array('f',[0])
  tree.SetBranchAddress("mcEventWeight",Branches["mcEventWeight"])
  Branches["jFEX_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEX_et8x8",Branches["jFEX_pt"])
  Branches["jFEX_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEX_eta",Branches["jFEX_eta"])
  Branches["jFEX_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEX_phi",Branches["jFEX_phi"])
  Branches["jFEXcalibrated_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEXcalibrated_et8x8",Branches["jFEXcalibrated_pt"])
  Branches["jFEXcalibrated_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEXcalibrated_phi",Branches["jFEXcalibrated_phi"])
  Branches["jFEXcalibrated_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jFEXcalibrated_eta",Branches["jFEXcalibrated_eta"])

from ROOT import *
import os,sys,argparse
from HelperClasses import *
from Arrays import *
from math import *
#import numpy as np

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--path',            action='store',      dest="path")
parser.add_argument('--outPATH',         action='store',      dest="outPATH")
parser.add_argument('--file',            action='store',      dest="inputFile")
parser.add_argument('--dataset',         action='store',      dest="dataset")
parser.add_argument('--debug',           action='store_true', dest="debug",       default=False)

args = parser.parse_args()

# Sample type
Dataset  = args.dataset

# Debugging
Debug = args.debug

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)
if args.path is None:
  print "ERROR: path to input file not provided, exiting"
  sys.exit(0)
if args.outPATH is None:
  print "ERROR: path to output files (outPATH) not provided, exiting"
  sys.exit(0)
if args.inputFile is None:
  print "ERROR: input file not provided, exiting"
  sys.exit(0)

#####################
# Output file name
#####################

inputFileName  = args.inputFile.replace(".root","")

OutName    = str(args.outPATH)
OutName   += "L1Hists_"
OutName   += Dataset
OutName   += "_" + inputFileName
if Debug:
  OutName += "_Debug"
OutName   += ".root"

#####################
# TTree name
#####################
TDirectoryName = "outTree"

# Max number of events for debuging
DebugMAXevents = 1000

#############################################
# Open TFile/TDirectory and loop over events
#############################################
print "INFO: Opening "+args.path+args.inputFile
tfile = TFile.Open(args.path+args.inputFile)
if not tfile:
  print "ERROR: "+args.path+args.inputFile+" not found, exiting"
  sys.exit(0)

tdir = TDirectoryFile()
tdir = tfile.Get(TDirectoryName)
if not tdir:
  print "ERROR: "+TDirectoryName+" not found, exiting"
  sys.exit(0)

# Get list of TTrees
Keys = tdir.GetListOfKeys()
keys = []
for key in Keys: keys.append(key.GetName())

# Set list of requested TTrees
keys = ['nominal']

# Loop over TTrees
for TTreeName in keys:

  # Open TFile
  print "INFO: Opening "+args.path+args.inputFile
  tfile = TFile.Open(args.path+args.inputFile)
  if not tfile:
    print "ERROR: "+args.path+args.inputFile+" not found, exiting"
    sys.exit(0)

  # Open corresponding directory
  tdir = TDirectoryFile()
  tdir = tfile.Get(TDirectoryName)
  if not tdir:
    print "ERROR: "+TDirectoryName+" not found, exiting"
    sys.exit(0)

  # Get TTree
  if Debug: print "DEBUG: Get "+TTreeName+" TTree"
  tree = tdir.Get(TTreeName)
  if not tree:
    print "ERROR: "+TTreeName+" not found, exiting"
    sys.exit(0)
  totalEvents = tree.GetEntries()
  print "INFO: TotalEvents in "+TTreeName+": "+str(totalEvents)

  ###################
  # Book histograms
  ###################

  # L1 kinematics
  NoCalibHistograms = dict()
  CalibHistograms   = dict()
  for i in range(0,8):
    hist = TH1D('L1nocalibPt{}'.format(i),'',7000,1,7001)
    hist.Sumw2()
    NoCalibHistograms[i] = hist
    hist   = TH1D('L1calibPt{}'.format(i),'',7000,1,7001)
    hist.Sumw2()
    CalibHistograms[i] = hist
  h_noCalibEta = TH1D('L1nocalibEta','',9,-4.5,4.5)
  h_noCalibEta.Sumw2()
  h_calibEta   = TH1D('L1calibEta','',9,-4.5,4.5)
  h_calibEta.Sumw2()

  ################
  # Set branches
  ################
  Branches = dict()
  setBranchStatus(tree)
  setBranchAddress(tree,Branches)

  ###################
  # Loop over events
  ###################
  for event_counter in xrange(0,totalEvents):

    # Get entry
    tree.GetEntry(event_counter)

    event_counter += 1
    if Debug and event_counter > DebugMAXevents:
      break # skip loop
    if Debug:
      print "DEBUG: Event counter: "+str(event_counter)
    if event_counter == 1:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
    if event_counter % 100000 == 0:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
 
    ####################
    # Get event weights
    ####################
    wgt = Branches["mcEventWeight"][0]
    if Debug:
      print "DEBUG: mcEventWeight: "+str(Branches["mcEventWeight"][0])
      print "DEBUG: final weight: "+str(wgt)

    #############################
    # Select uncalibratedL1 jets
    #############################
    SelectedNoCalibL1Jets = []
    if Debug: print('Select uncalibrated L1 jets:')
    for ijet in xrange(0,len(Branches['jFEX_pt'])):
      jetPt  = Branches['jFEX_pt'][ijet]
      jetEta = Branches['jFEX_eta'][ijet]
      jetPhi = Branches['jFEX_phi'][ijet]
      if Debug: print('DEBUG: L1 uncalibrated jet pt = {}'.format(jetPt))
      # protection
      if jetPt < 0: continue # skip negative et8x8 L1 jet
      if abs(jetEta) > 3.1: continue # skip forward L1 jets
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiM(jetPt,jetEta,jetPhi,0) # L1 jets are massless
      SelectedNoCalibL1Jets.append(TLVjet)
    if Debug: print('DEBUG: Number of selected uncalibrated L1 jets = {}'.format(len(SelectedNoCalibL1Jets)))
    if Debug: print('DEBUG: Selected uncalibrated L1 jets:')
    for jet in SelectedNoCalibL1Jets:
      if Debug: print('DEBUG: Jet pt = {}'.format(jet.Pt()))

    #############################
    # Select calibratedL1 jets
    #############################
    SelectedCalibL1Jets = []
    if Debug: print('Select calibrated L1 jets:')
    for ijet in xrange(0,len(Branches['jFEXcalibrated_pt'])):
      jetPt  = Branches['jFEXcalibrated_pt'][ijet]
      jetEta = Branches['jFEXcalibrated_eta'][ijet]
      jetPhi = Branches['jFEXcalibrated_phi'][ijet]
      if Debug: print('DEBUG: L1 calibrated jet pt = {}'.format(jetPt))
      # protection
      if jetPt < 0: continue # skip negative et8x8 L1 jet
      if abs(jetEta) > 3.1: continue # skip forward L1 jets
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiM(jetPt,jetEta,jetPhi,0) # L1 jets are massless
      if Debug: print('DEBUG: Calibrated Energy = {}'.format(TLVjet.E()))
      if Debug: print('DEBUG: Calibrated pt = {}'.format(TLVjet.Pt()))
      if Debug: print('DEBUG: Calibrated Et = {}'.format(TLVjet.Et()))
      SelectedCalibL1Jets.append(TLVjet)
    if Debug: print('DEBUG: Number of selected calibrated L1 jets = {}'.format(len(SelectedCalibL1Jets)))
    if Debug: print('DEBUG: Selected calibrated L1 jets:')
    for jet in SelectedCalibL1Jets:
      if Debug: print('DEBUG: Jet pt = {}'.format(jet.Pt()))

    #############################################
    # Fill distributions
    #############################################

    if Debug: print('DEBUG: Filling distributions')

    SelectedCalibL1Jets.sort(key=lambda x: x.Pt(), reverse=True)
    SelectedNoCalibL1Jets.sort(key=lambda x: x.Pt(), reverse=True)
    # Uncalibrated jets
    for ijet in range(0,len(SelectedNoCalibL1Jets)):
      h_noCalibEta.Fill(SelectedNoCalibL1Jets[ijet].Eta())
      if ijet > 7: continue
      NoCalibHistograms[ijet].Fill(SelectedNoCalibL1Jets[ijet].Pt())
    # Calibrated jets
    for ijet in range(0,len(SelectedCalibL1Jets)):
      h_calibEta.Fill(SelectedCalibL1Jets[ijet].Eta())
      if ijet > 7: continue
      CalibHistograms[ijet].Fill(SelectedCalibL1Jets[ijet].Pt())

  ##################################
  # Write histograms to a ROOT file
  ##################################
  outFile = TFile(OutName,"UPDATE")
  for key,hist in NoCalibHistograms.iteritems():
    hist.Write()
  for key,hist in CalibHistograms.iteritems():
    hist.Write()
  h_noCalibEta.Write()
  h_calibEta.Write()
  outFile.Close()
  print "Histograms saved to "+OutName
print ">>> DONE <<<"
