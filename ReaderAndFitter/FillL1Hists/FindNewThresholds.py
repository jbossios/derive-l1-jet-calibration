from ROOT import *
import os,sys

Chains = [
  "L1_jJ15",
  "L1_jJ20",
  "L1_jJ50",
  "L1_jJ75",
  "L1_jJ100",
  "L1_jJ120",
  'L1_3jJ50',
  'L1_4jJ15',
  'L1_4jJ20',
  'L1_5jJ15',
  'L1_6jJ15',
]

FileName = 'Testing/L1Hists_HH4b_HH4b_450000_tree.root'
File     = TFile.Open(FileName)
if not File:
  print('ERROR: {} not found, exiting'.format(FileName))
  sys.exit(0)

nBins = 7000

####################
# Loop over chains
####################
Thresholds = dict()
for chain in Chains:
  # Find index (multiplicity), example jJ4 -> 4 which represents 5 jet
  if '_j' in chain:
    index     = 0
  else:
    index     = int(chain.split('jJ')[0].split('_')[1])-1
  # Find number of fired events for uncalibrated case
  threshold   = int(chain.split('J')[1])
  uncalibHist = File.Get('L1nocalibPt{}'.format(index))
  RateToMatch = uncalibHist.Integral(threshold,nBins)
  if RateToMatch < 0:
    print('WARNING: negative rate for {} = {}'.format(chain,RateToMatch))
  # Find threshold for calibrated jets for which the number of fired events is close to the uncalibrated case
  calibHist = File.Get('L1calibPt{}'.format(index))
  Min = 1E7
  for th in range(1,300):
    integral = calibHist.Integral(th,nBins)
    Diff     = abs(RateToMatch-integral)
    if Diff < Min:
      MatchedThreshold = th
      Min  = Diff
      Rel  = Diff/RateToMatch
  print('Chosen threshold for {} = {}'.format(chain,MatchedThreshold))
  if Rel > 0.01:
    print('WARNING: Difference larger than 1% for {} = {} ({}%)'.format(chain,Diff,Rel))
  Thresholds[chain] = MatchedThreshold

# Show results
for chain in Chains:
  print('{} = {}'.format(chain,Thresholds[chain]))

print('>>> ALL DONE <<<')
    
 
