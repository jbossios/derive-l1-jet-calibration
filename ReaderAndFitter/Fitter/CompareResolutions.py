###########################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    17/09/2020
## Purpose: Derive L1 jet calibration
###########################################################################################

Inputs = {
  'Uncalibrated' : 'Outputs/Histograms_180920.root',
  'Calibrated'   : 'Outputs/HistogramsCalibrated_220920.root',
}

###########################################################################################
# DO NOT MODIFY (below this line)
###########################################################################################

from ROOT import *
import os,sys

# AtlasStyle
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()
gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../Reader') # insert at 1, 0 is the script path
from Arrays import *

Colors = {
  'Uncalibrated' : kBlack,
  'Calibrated'   : kRed,
}

# Definitions for legends
x0   = 0.7
pos0 = 0.86      # y-axis
pos1 = 0.86-0.05 # y-axis
pos2 = 0.86-0.1    # y-axis

# Plot resolution vs pttruth for each eta bin
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/CompareResolutions_vs_pttruth.pdf'
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
for etabin in xrange(0,nEtaBins): # loop over eta bins
  Stack = THStack()
  for case in Inputs: # loop over cases
    # Open file
    File = TFile.Open(Inputs[case])
    if not File:
      print('ERROR: {} not found, exiting'.format(Inputs[case]))
      sys.exit(0)
    # Get histogram
    HistName = 'Resolution_vs_pttruth_eta_{}'.format(etabin)
    Hist     = File.Get(HistName)
    if not Hist:
      print('ERROR: {} not found in {}'.format(HistName,Inputs[case]))
      sys.exit(0)
    Hist.SetDirectory(0)
    Hist.SetLineColor(Colors[case])
    Hist.SetMarkerColor(Colors[case])
    Hist.SetMinimum(-1)
    Hist.SetMaximum(2.4)
    Stack.Add(Hist,'p')
    File.Close()
  Stack.Draw('nostack')
  Stack.GetYaxis().SetTitle('Jet resolution')
  Stack.GetXaxis().SetTitle('p_{T}^{True} [GeV]')
  Stack.GetXaxis().SetTitleOffset(1.1)
  Stack.GetYaxis().SetTitleOffset(1.1)
  text0      = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}"
  TextBlock0 = TLatex(x0,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  Canvas.Print(outPDF)
Canvas.Print(outPDF+']')


print('>>> ALL DONE <<<')
