###########################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    17/09/2020
## Purpose: Derive L1 jet calibration
###########################################################################################

# Date of inputs
Date = '220920'

# Jets calibrated?
Calibrated = True

# pttruth bins
#PtBins   = [20,30,40,50,80,110,150,300,500]
PtBins = [20,30,40,50,65,80,110,150,300,500]

# L1 pt bins
L1PtBins = [10,20,30,40,50,65,80,110,150,300]

###########################################################################################
# DO NOT MODIFY (below this line)
###########################################################################################

calib = 'Calibrated' if Calibrated else ''

if not Calibrated: Date = '180920'

FileName = 'Inputs/Dijets{}_All_{}.root'.format(calib,Date)

improveFits = False

import os,sys,array
from ROOT import *
os.system('mkdir Plots Outputs')

# Import things from Reader
sys.path.insert(1, '../Reader') # insert at 1, 0 is the script path
from Arrays import *

# pttruth binning
nPtBins = len(PtBins)-1
PtArray = array.array('d',PtBins)

# L1 pt binning
nL1PtBins = len(L1PtBins)-1
L1PtArray = array.array('d',L1PtBins)

# Eta binning
EtaArray = array.array('d',EtaBins)

# Eta bins for response comparisons
CompareEtaBins = [2,6,9,11,16] # [-3.5,-3], [-1.5,-1], [0,0.5], [1,1.5], [3,3.5]

# pt bins for response comparisons
ComparePtBins = [3,6,8] # [50,65], [110,150], [300,500]

# Setup for re-fitting
nRangeTypes = 5
rangeType   = [1,2,3,4,5] # Two-side extended/shorter, Left-side extended/shorter, Right-side extended/shorter, Moved to left, Moved to the right
nRangeDelta = 20
rangeDelta  = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0] # %RMS
nFits       = nRangeTypes*nRangeDelta # number of possible fits

# AtlasStyle
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()
gROOT.SetBatch(True)

#############
# Functions #
#############

def GetFitRange(proj, Type, Delta):
  # Default (Type==0)
  Min = proj.GetMean() - proj.GetRMS()*0.9
  Max = proj.GetMean() + proj.GetRMS()*0.9
  if Type == 1: # Two-side extended/shorter by Delta
    Min = proj.GetMean() - proj.GetRMS()*Delta
    Max = proj.GetMean() + proj.GetRMS()*Delta
  elif Type == 2: # Left-side extended/shorter by Delta
    Min = proj.GetMean() - proj.GetRMS()*Delta
  elif Type == 3: # Right-side extended by Delta
    Max = proj.GetMean() + proj.GetRMS()*Delta
  elif Type == 4: # Move range to the left by Delta
    Min = proj.GetMean() - proj.GetRMS()*Delta
    Max = proj.GetMean() - proj.GetRMS()*Delta
  elif Type == 5: # Move range to the right by Delta
    Min = proj.GetMean() + proj.GetRMS()*Delta
    Max = proj.GetMean() + proj.GetRMS()*Delta
  else: print('ERROR: Wrong type in GetFitRange()')
  return Min,Max

#################################
# Initialize JES_ResponseFitter #
#################################
from JES_BalanceFitter import *
Nsigma = 1.5
Fitter = JES_BalanceFitter(Nsigma)
Fitter.SetGaus()
Fitter.SetFitColor(kBlue)
Fitter.fitOpt = 'REQ'

###################
# Book Histograms #
###################
h_Response   = dict() # average response vs pttruth for each eta bin
h_Resolution = dict() # resolution vs pttruth for each eta bin
for etabin in xrange(0,nEtaBins): # loop over eta bins
  name = 'AvgResponse_vs_pttruth_eta_{}'.format(etabin)
  hist = TH1D(name,'',nPtBins,PtArray) 
  h_Response[etabin] = hist
  name = 'Resolution_vs_pttruth_eta_{}'.format(etabin)
  hist = TH1D(name,'',nPtBins,PtArray) 
  h_Resolution[etabin] = hist

###################
# Open input file #
###################
File = TFile.Open(FileName)
if not File:
  print('ERROR: {} not found, exiting'.format(FileName))
  sys.exit(0)

########################
# Get input histograms #
########################
Histograms = dict()
for etabin in xrange(0,nEtaBins):
  name = 'Response_vs_pttruth_eta_{}'.format(etabin)
  hist = File.Get(name)
  if not hist:
    print('ERROR: {} not found, exiting'.format(name))
    sys.exit(0)
  hist.SetDirectory(0)
  Histograms[etabin] = hist

PtReco_vs_PtTruth = dict()
for etabin in xrange(0,nEtaBins):
  name = 'ptreco_vs_pttruth_eta_{}'.format(etabin)
  hist = File.Get(name)
  if not hist:
    print('ERROR: {} not found, exiting'.format(name))
    sys.exit(0)
  hist.SetDirectory(0)
  PtReco_vs_PtTruth[etabin] = hist

PtTruth_vs_PtTruth = dict()
for etabin in xrange(0,nEtaBins):
  name = 'pttruth_vs_pttruth_eta_{}'.format(etabin)
  hist = File.Get(name)
  if not hist:
    print('ERROR: {} not found, exiting'.format(name))
    sys.exit(0)
  hist.SetDirectory(0)
  PtTruth_vs_PtTruth[etabin] = hist
File.Close()

#################
# Fit Gaussians #
#################

# Make the corresponding projections and fit the response distributions with a Gaussian
h_Projections = dict()

# Create canvas to save each response distribution and its fit
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/Fits{}.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
# Definitions for legends
x0   = 0.7
pos0 = 0.86      # y-axis
pos1 = 0.86-0.05 # y-axis
pos2 = 0.86-0.1    # y-axis

# Loop over eta bins
for etabin in xrange(0,nEtaBins):
  h_Projections[etabin] = []
  # Loop over pt bins
  for ptbin in xrange(0,nPtBins):
    # Get projection
    name    = 'Projection_eta_{}_pt_{}_{}'.format(etabin,PtBins[ptbin],PtBins[ptbin+1])
    LowerEdge = Histograms[etabin].GetXaxis().FindBin(PtBins[ptbin])
    UpperEdge = Histograms[etabin].GetXaxis().FindBin(PtBins[ptbin+1])-1
    h_Projections[etabin].append(Histograms[etabin].ProjectionY(name,LowerEdge,UpperEdge,'e'))
    # Apply style
    h_Projections[etabin][ptbin].GetXaxis().SetTitleOffset(1.1)
    h_Projections[etabin][ptbin].SetXTitle('Response')
    h_Projections[etabin][ptbin].SetMarkerColor(kBlack)
    h_Projections[etabin][ptbin].Draw()
    # Fit distributions with enough stats
    if h_Projections[etabin][ptbin].GetEntries() > 80:
      # Fit range
      Min = h_Projections[etabin][ptbin].GetMean() - h_Projections[etabin][ptbin].GetRMS()*0.9
      Max = h_Projections[etabin][ptbin].GetMean() + h_Projections[etabin][ptbin].GetRMS()*0.9
      # Fit
      Fitter.Fit(h_Projections[etabin][ptbin],Min,Max)
      Mean       = Fitter.GetMean()
      MeanError  = Fitter.GetMeanError()
      Sigma      = Fitter.GetSigma()
      SigmaError = Fitter.GetSigmaError()
      Fitter.DrawFitAndHisto(0,1.5)
      # Evaluate goodness of the fit and re-fit if needed
      errorValues = []
      chiValues   = []
      meanValues  = []
      typeValues  = []
      deltaValues = []
      fitImproved = False
      if MeanError > 0.02 and improveFits: # bad quality of fit
        print('Re-fitting...')
        # Try different ranges and save outcome
        for iType in xrange(0,nRangeTypes):
          for iDelta in xrange(0,nRangeDelta):
            Min,Max = GetFitRange(h_Projections[etabin][ptbin],rangeType[iType],rangeDelta[iDelta])
            Fitter.Fit(h_Projections[etabin][ptbin],Min,Max)
            errorValues.append(Fitter.GetMeanError())
            chiValues.append(Fitter.GetChi2Ndof())
            meanValues.append(Fitter.GetMean())
            typeValues.append(iType)
            deltaValues.append(iDelta)
        # Pick fit with lower error-chi2/Ndof
        bestType        = 0
        bestDelta       = 8
        minErr          = 1E9
        minChi2overNdof = 1E9
        if nFits != len(typeValues): print('ERROR: nFits != len(typeValues()')
        for ierror in xrange(0,len(errorValues)):
          if errorValues[ierror] < minErr and chiValues[ierror] < minChi2overNdof and meanValues[ierror] < 2 and meanValues[ierror] > -1 and errorValues[ierror] < 1:
            bestType        = typeValues[ierror]
            bestDelta       = deltaValues[ierror]
            minErr          = errorValues[ierror]
            minChi2overNdof = chiValues[ierror]
        if minChi2overNdof > 1000: # Use fit with minimum error instead
          bestType  = 0
          bestDelta = 8
          minErr    = 1E9
          for ierror in xrange(0,len(errorValues)):
            if errorValues[ierror] < minErr and meanValues[ierror] < 2 and meanValues[ierror] > -1 and errorValues[ierror] < 1:
              bestType  = typeValues[ierror]
              bestDelta = deltaValues[ierror]
              minErr    = errorValues[ierror]
        # Use best fit
        if rangeType[bestType] != 1 and rangeDelta[bestDelta] != 0.9:
          fitImproved = True
          print('Fit was improved!')
        Min,Max = GetFitRange(h_Projections[etabin][ptbin],rangeType[bestType],rangeDelta[bestDelta])
        Fitter.Fit(h_Projections[etabin][ptbin],Min,Max)
        Mean      = Fitter.GetMean()
        MeanError = Fitter.GetMeanError()
        Sigma      = Fitter.GetSigma()
        SigmaError = Fitter.GetSigmaError()
    # Add legends
    text0 = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}";
    text1 = "#scale[0.7]{"+str(PtBins[ptbin])+" #leq  p_{T}^{True} [GeV] < "+str(PtBins[ptbin+1])+"}"
    text2 = "#scale[0.7]{Fit improved}"
    TextBlock0 = TLatex(x0,pos0,text0)
    TextBlock1 = TLatex(x0,pos1,text1)
    TextBlock2 = TLatex(x0,pos2,text2)
    TextBlock0.SetNDC()
    TextBlock1.SetNDC()
    TextBlock2.SetNDC()
    TextBlock0.Draw("same")
    TextBlock1.Draw("same")
    if fitImproved: TextBlock2.Draw("same") # Temporary
    Canvas.Print(outPDF)
    # Fill average response histogram
    if Mean !=0:
      h_Response[etabin].SetBinContent(ptbin+1,Mean);
      h_Response[etabin].SetBinError(ptbin+1,MeanError);
      h_Resolution[etabin].SetBinContent(ptbin+1,Sigma/Mean);
      h_Resolution[etabin].SetBinError(ptbin+1,SigmaError/Mean + (Sigma*MeanError)/(Mean*Mean));
Canvas.Print(outPDF+']')
    
# Save average response vs pttruth histograms
outFile = TFile('Outputs/Histograms{}_{}.root'.format(calib,Date),'RECREATE')
for key,hist in h_Response.iteritems():
  hist.Write()
for key,hist in h_Resolution.iteritems():
  hist.Write()
outFile.Close()

#########################
# Make response figures #
#########################

# Plot response vs pttruth for each eta bin
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/Response{}_vs_pttruth.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
for etabin in xrange(0,nEtaBins):
  h_Response[etabin].Draw('p')
  h_Response[etabin].SetYTitle('Average jet response')
  h_Response[etabin].SetXTitle('p_{T}^{True} [GeV]')
  h_Response[etabin].GetXaxis().SetTitleOffset(1.1)
  h_Response[etabin].GetYaxis().SetTitleOffset(1.1)
  text0      = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}"
  TextBlock0 = TLatex(x0,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  if Calibrated: # draw a line at 1
    Line = ROOT.TLine(PtBins[0],1,PtBins[nPtBins],1)
    Line.SetLineColor(kRed)
    Line.SetLineStyle(7)
    Line.Draw("same") 
  Canvas.Print(outPDF)
Canvas.Print(outPDF+']')

# Compare response vs pttruth for a few eta bins
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/Response{}_vs_pttruth_etaComparison.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
counter = 0
Colors  = [kBlack,kBlue,kRed,kCyan,kOrange]
Legends = TLegend(0.75,0.2,0.9,0.5)
Stack   = THStack()
for etabin in CompareEtaBins:
  h_Response[etabin].SetLineColor(Colors[counter])
  h_Response[etabin].SetMarkerColor(Colors[counter])
  Stack.Add(h_Response[etabin],"p")
  Legends.AddEntry(h_Response[etabin],str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1)),'p')
  counter += 1
Stack.Draw('nostack')
Stack.GetXaxis().SetTitleOffset(1.1)
Stack.GetYaxis().SetTitleOffset(1.1)
Stack.GetYaxis().SetTitle('Average jet response')
Stack.GetXaxis().SetTitle('p_{T}^{True} [GeV]')
Legends.Draw("same")
Canvas.Print(outPDF)
Canvas.Print(outPDF+']')

# Compare response vs eta for a few pttruth bins
# First prepare histogram
Response_vs_eta = dict()
for ptbin in ComparePtBins:
  Response_vs_eta[ptbin] = TH1D('Response_vs_eta_pt_{}'.format(ptbin),'',nEtaBins,EtaArray)
  for etabin in xrange(0,nEtaBins):
    if h_Response[etabin].GetBinContent(ptbin+1) != 0:
      Response_vs_eta[ptbin].SetBinContent(etabin+1,h_Response[etabin].GetBinContent(ptbin+1))
      Response_vs_eta[ptbin].SetBinError(etabin+1,h_Response[etabin].GetBinError(ptbin+1))
# Now produce plots
for ptbin in ComparePtBins:
  Canvas = TCanvas()
  Canvas.SetMargin(0.12,0.04,0.12,0.04)
  outPDF = 'Plots/Response{}_vs_eta_pttruth_{}_{}.pdf'.format(calib,PtBins[ptbin],PtBins[ptbin+1])
  Canvas.Print(outPDF+'[')
  Canvas.SetLogy(0)
  Response_vs_eta[ptbin].Draw('HIST][')
  Response_vs_eta[ptbin].SetYTitle('Average jet response')
  Response_vs_eta[ptbin].SetXTitle('#eta')
  Response_vs_eta[ptbin].GetXaxis().SetTitleOffset(1.1)
  Response_vs_eta[ptbin].GetYaxis().SetTitleOffset(1.1)
  text0      = "#scale[0.7]{"+str(PtBins[ptbin])+" #leq  p_{T}^{True} [GeV] < "+str(PtBins[ptbin+1])+"}"
  TextBlock0 = TLatex(0.4,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  Canvas.Print(outPDF)
  Canvas.Print(outPDF+']')

#########################
# Make resolution plots #
#########################

# Plot resolution vs pttruth for each eta bin
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/Resolution{}_vs_pttruth.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
for etabin in xrange(0,nEtaBins):
  h_Resolution[etabin].Draw('p')
  h_Resolution[etabin].SetYTitle('Jet resolution')
  h_Resolution[etabin].SetXTitle('p_{T}^{True} [GeV]')
  h_Resolution[etabin].GetXaxis().SetTitleOffset(1.1)
  h_Resolution[etabin].GetYaxis().SetTitleOffset(1.1)
  h_Resolution[etabin].SetMinimum(-1)
  h_Resolution[etabin].SetMaximum(2.4)
  text0      = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}"
  TextBlock0 = TLatex(x0,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  Canvas.Print(outPDF)
Canvas.Print(outPDF+']')

#####################################
# Get average response vs L1 jet pt #
#####################################

#############
# Procedure
#############
# Each h_Response histogram (response vs pttruth for each eta bin) plots the average response vs pttruth
# That response corresponds to the average pttruth of each bin and not the center of the bin
# Then we need <L1pt> vs pttruth, such that we can know <L1pt> for the average pttruth of each pttruth bin
# Then we need to interpolate and find the coresponding L1pt for each center of each L1pt bin
# Then we need a TH2D of response vs L1 pt and eta

##################################################################################
# 1) Get <pttruth> for each pttruth bin center (for the bins used in the fitter)
AvgPtTruth = dict()
for etabin in xrange(0,nEtaBins):
  AvgPtTruth[etabin] = dict()
  for ptbin in xrange(0,nPtBins):
    # Get pttruth distribution for each pttruth bin
    name                      = 'TruthProfile_eta_{}_pttruth_{}_{}'.format(etabin,PtBins[ptbin],PtBins[ptbin+1])
    LowerEdge                 = PtTruth_vs_PtTruth[etabin].GetXaxis().FindBin(PtBins[ptbin])
    UpperEdge                 = PtTruth_vs_PtTruth[etabin].GetXaxis().FindBin(PtBins[ptbin+1])-1
    Projection                = PtTruth_vs_PtTruth[etabin].ProjectionY(name,LowerEdge,UpperEdge,'e')
    AvgPtTruth[etabin][ptbin] = Projection.GetMean()

##################################################################################
# 2) Get <L1pt> vs pttruth
PtReco_vs_PtTruth_1D = dict()
for etabin in xrange(0,nEtaBins):
  PtReco_vs_PtTruth_1D[etabin] = TH1D('PtReco_vs_PtTruth_eta_{}'.format(etabin),'',nPtBins,PtArray)
  for ptbin in xrange(0,nPtBins):
    # Get ptreco distribution for each pttruth bin
    name                     = 'RecoProfile_eta_{}_pt_{}_{}'.format(etabin,PtBins[ptbin],PtBins[ptbin+1])
    LowerEdge                = PtReco_vs_PtTruth[etabin].GetXaxis().FindBin(PtBins[ptbin])
    UpperEdge                = PtReco_vs_PtTruth[etabin].GetXaxis().FindBin(PtBins[ptbin+1])-1
    Projection               = PtReco_vs_PtTruth[etabin].ProjectionY(name,LowerEdge,UpperEdge,'e')
    PtReco_vs_PtTruth_1D[etabin].SetBinContent(ptbin+1,Projection.GetMean())
    PtReco_vs_PtTruth_1D[etabin].SetBinError(ptbin+1,Projection.GetMeanError())

# Save PDF plots
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/AvgL1pt{}_vs_pttruth.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
for etabin in xrange(0,nEtaBins):
  PtReco_vs_PtTruth_1D[etabin].Draw('p')
  PtReco_vs_PtTruth_1D[etabin].SetYTitle('Average L1 jet pt')
  PtReco_vs_PtTruth_1D[etabin].SetXTitle('p_{T}^{true} [GeV]')
  PtReco_vs_PtTruth_1D[etabin].GetXaxis().SetTitleOffset(1.1)
  PtReco_vs_PtTruth_1D[etabin].GetYaxis().SetTitleOffset(1.1)
  text0      = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}"
  TextBlock0 = TLatex(x0,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  Canvas.Print(outPDF)
Canvas.Print(outPDF+']')

##################################################################################
# 3) Interpolate histogram from 2) to get <L1pt> for each <pttruth> value
AvgPtReco_4each_AvgPtTruth = dict()
Responses                  = dict()
ResponseErrors             = dict()
for etabin in xrange(0,nEtaBins):
  AvgPtReco_4each_AvgPtTruth[etabin] = []
  Responses[etabin]                  = []
  ResponseErrors[etabin]             = []
  for ptbin in xrange(0,nPtBins):
    # Get avgptreco for each avgpttruth
    AvgPtReco_4each_AvgPtTruth[etabin].append(PtReco_vs_PtTruth_1D[etabin].Interpolate(AvgPtTruth[etabin][ptbin]))
    # Get reponse for each pttruth bin (needed for later)
    Responses[etabin].append(h_Response[etabin].GetBinContent(ptbin+1))
    ResponseErrors[etabin].append(h_Response[etabin].GetBinError(ptbin+1))
 
##################################################################################
# 4) Create a TGraph with response for each pttruth bin using as x-value the corresponding <L1pt> value
AvgResponse_vs_L1pt_TGraph = dict()
for etabin in xrange(0,nEtaBins):
  AvgResponse_vs_L1pt_TGraph[etabin] = TGraph(nPtBins,array.array('d',AvgPtReco_4each_AvgPtTruth[etabin]),array.array('d',Responses[etabin]))

##################################################################################
# 5) Create a histogram such that we have the response in the original ptbinning evaluating the response from the TGraph at each bin center
AvgResponse_vs_L1pt_1D = dict()
for etabin in xrange(0,nEtaBins):
  AvgResponse_vs_L1pt_1D[etabin] = TH1D('AvgResponse_vs_L1pt_eta_{}'.format(etabin),'',nL1PtBins,L1PtArray)
  for ptbin in xrange(0,nL1PtBins):
    AvgResponse_vs_L1pt_1D[etabin].SetBinContent(ptbin+1,AvgResponse_vs_L1pt_TGraph[etabin].Eval(AvgResponse_vs_L1pt_1D[etabin].GetBinCenter(ptbin+1)))
    AvgResponse_vs_L1pt_1D[etabin].SetBinError(ptbin+1,ResponseErrors[etabin][ptbin]) # using errors from original response vs pttruth

# Plot response vs ptreco for each eta bin
Canvas = TCanvas()
Canvas.SetMargin(0.12,0.04,0.12,0.04)
outPDF = 'Plots/Response{}_vs_L1pt.pdf'.format(calib)
Canvas.Print(outPDF+'[')
Canvas.SetLogy(0)
for etabin in xrange(0,nEtaBins):
  AvgResponse_vs_L1pt_1D[etabin].Draw('p')
  AvgResponse_vs_L1pt_1D[etabin].SetYTitle('Average jet response')
  AvgResponse_vs_L1pt_1D[etabin].SetXTitle('p_{T}^{L1} [GeV]')
  AvgResponse_vs_L1pt_1D[etabin].GetXaxis().SetTitleOffset(1.1)
  AvgResponse_vs_L1pt_1D[etabin].GetYaxis().SetTitleOffset(1.1)
  text0      = "#scale[0.7]{"+str(round(EtaBins[etabin],1))+" #leq  #eta < "+str(round(EtaBins[etabin+1],1))+"}"
  TextBlock0 = TLatex(x0,pos1,text0)
  TextBlock0.SetNDC()
  TextBlock0.Draw("same")
  Canvas.Print(outPDF)
Canvas.Print(outPDF+']')

##################################################################################
# 6) From each response vs L1 pt histogram, create a TH2 of response vs L1 pt and eta
AvgResponse_vs_L1pt_eta = TH2D('AvgResponse_vs_L1pt_eta','',nL1PtBins,L1PtArray,nEtaBins,EtaArray)
FinalResponses = dict()
for etabin in xrange(0,nEtaBins):
  FinalResponses[etabin] = []
  for ptbin in xrange(0,nL1PtBins):
    FinalResponses[etabin].append(AvgResponse_vs_L1pt_1D[etabin].GetBinContent(ptbin+1))
    AvgResponse_vs_L1pt_eta.SetBinContent(ptbin+1,etabin+1,AvgResponse_vs_L1pt_1D[etabin].GetBinContent(ptbin+1))
    AvgResponse_vs_L1pt_eta.SetBinError(ptbin+1,etabin+1,AvgResponse_vs_L1pt_1D[etabin].GetBinError(ptbin+1))
# Save TH2D to a Root file
outFile = TFile('Outputs/TH2DResponse{}_vs_L1pt_eta_{}.root'.format(calib,Date),'RECREATE')
AvgResponse_vs_L1pt_eta.Write()
outFile.Close()

##################################################################################
## 7) Smoothing (1st create a TGraph2DErrors and then smooth) TODO
#
## Create TGraph2DErrors()
#AvgResponseGraph = TGraph2DErrors()
#AvgResponseGraph.SetName('AvgResponseGraph')
#counter = 0
#for etabin in xrange(0,nEtaBins):
#  FinalResponses[etabin] = []
#  for ptbin in xrange(0,nL1PtBins):
#    x = AvgResponse_vs_L1pt_eta.GetXaxis().GetBinCenter(ptbin+1)
#    y = AvgResponse_vs_L1pt_eta.GetYaxis().GetBinCenter(etabin+1)
#    AvgResponseGraph.SetPoint(counter,x,y,AvgResponse_vs_L1pt_eta.GetBinContent(ptbin+1,etabin+1))
#    AvgResponseGraph.SetPointError(counter,x,y,AvgResponse_vs_L1pt_eta.GetBinError(ptbin+1,etabin+1))
#    counter += 0
#
## Smooth 2D graph
#
## Prepare final TH2D histogram with smoothed calibration

print('>>> ALL DONE <<<')
