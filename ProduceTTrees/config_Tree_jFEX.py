import ROOT
from xAODAnaHelpers import Config     as xAH_config

c = xAH_config()

msgLevel = "info"

# Make basic event selections
c.algorithm("BasicEventSelection", {
  "m_name"                        : "BasicSelection",
  "m_msgLevel"                    : msgLevel,
  "m_applyGRLCut"                 : False,
  "m_useMetaData"                 : True,
  "m_storePassHLT"                : False,
  "m_storeTrigDecisions"          : False,
  "m_applyTriggerCut"             : False,
  "m_triggerSelection"            : "",
  "m_checkDuplicatesMC"           : True,
  "m_applyJetCleaningEventFlag"   : True,
  "m_PVNTrack"                    : 2,
  "m_applyPrimaryVertexCut"       : True,
  "m_vertexContainerName"         : "PrimaryVertices",
  "m_doPUreweighting"             : False,
  } )

# Reconstruct truth jets
c.algorithm("TruthJetReconstruction", {
  "m_name" : "truthJetReconstructionAlg",
  } )

# Select truth jets
c.algorithm("JetSelector", {
  "m_name"                           : "TruthJetSelector",
  "m_msgLevel"                       : msgLevel,
  "m_pT_min"                         : 7e3,
  "m_inContainerName"                : "AntiKt4TruthJets",
  "m_outContainerName"               : "TruthJets_selected",
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

# Save L1 and truth jets to a TTree
c.algorithm("TreeAlgo", {
  "m_name"                        : "TreeAlgo",
  "m_msgLevel"                    : msgLevel,
  "m_l1JetContainerName"          : "jRoundJets",
  "m_truthJetContainerName"       : "TruthJets_selected",
  "m_truthJetDetailStr"           : "kinematic",
  } )

