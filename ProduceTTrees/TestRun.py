import os,sys

sample = '/eos/atlas/atlascerngroupdisk/trig-jet/AOD_jFEXsamples/mc16_13TeV/AOD.21577847._000775.pool.root.1'

#config = "config_Tree_jFEX"
config = "config_Tree_jFEX_calibrated"

################################################
## DO NOT MODIFY
################################################

# choose appropiate config
CONFIG  = config
CONFIG += ".py"

# run xAH_run.py
command  = "python source/xAODAnaHelpers/scripts/xAH_run.py --config source/xAODAnaHelpers/data/"
command += CONFIG
command += " --files "
command += sample
command += " --force direct"
print(command)
os.system(command)
