# create folders

mkdir build source

# get xAH
cd source
git clone git@github.com:UCATLAS/xAODAnaHelpers.git

# get truthJetReconstructionAlg
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/truthjetreconstructionalg.git

# get L1JetCalibrator
git clone ssh://git@gitlab.cern.ch:7999/jbossios/l1jetcalibrator.git
cd ..

# move xAH configs to proper location
mv config_Tree_*.py source/xAODAnaHelpers/data/
