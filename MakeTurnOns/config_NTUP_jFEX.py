# PLAN: save information to be able to do the following:
# Emulate several L1_jJ chains using calibrated and uncalibrated jFEX jets
# L1_jJ20  (reference HLT_j175)
# L1_jJ100 (reference HLT_j175)
# L1_4jJ15 (reference HLT_4j45)
# make turn-on curves as a function of HLT jet pt and offline jet pt

import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

msgLevel = "info"

# Basic event selection 
c.algorithm("BasicEventSelection", { 
  "m_name"                      : "BasicSelection",
  "m_msgLevel"                  : msgLevel,
  "m_applyGRLCut"               : False,
  "m_useMetaData"               : True,
  "m_storePassHLT"              : True,
  "m_storeTrigDecisions"        : True,
  "m_applyTriggerCut"           : True,
  "m_triggerSelection"          : "(HLT|L1)_[1-9]?0?(j|J)[0-9]+.*",
  "m_checkDuplicatesMC"         : True,
  "m_applyEventCleaningCut"     : True,
  "m_applyJetCleaningEventFlag" : True,
  "m_applyPrimaryVertexCut"     : True,
  "m_vertexContainerName"       : "PrimaryVertices",
  "m_PVNTrack"                  : 2,
  "m_doPUreweighting"           : False,
  } )

# Calibrate offline small-R PFlow jets
c.algorithm("JetCalibrator", {
  "m_name"                    : "JetCalibrator",
  "m_msgLevel"                : msgLevel,
  "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config",
  "m_calibSequence"           : "JetArea_Residual_EtaJES_GSC_Smear",
  "m_sort"                    : True,
  "m_redoJVT"                 : True,
  "m_inContainerName"         : "AntiKt4EMPFlowJets",
  "m_outContainerName"        : "Jets_Calibrated",
  "m_jetAlgo"                 : "AntiKt4EMPFlow",
  "m_outputAlgo"              : "Jets_Calibrated_Algo",
  "m_doCleaning"              : False,
  "m_jetCleanUgly"            : False,
  } )
# Select calibrated offline small-R PFlow jets
c.algorithm("JetSelector", {
  "m_name"                    : "JetSelector",
  "m_msgLevel"                : msgLevel,
  "m_decorateSelectedObjects" : False,
  "m_cleanJets"               : True,
  "m_doJVT"                   : True,
  "m_noJVTVeto"               : False, # discard jets not passing JVT
  "m_haveTruthJets"           : False,
  "m_createSelectedContainer" : True,
  "m_inContainerName"         : "Jets_Calibrated",
  "m_outContainerName"        : "Jets_Selected",
  } )

# Calibrate offline small-R EMTopo jets
c.algorithm("JetCalibrator", {
  "m_name"                    : "EMJetCalibrator",
  "m_msgLevel"                : msgLevel,
  "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config",
  "m_calibSequence"           : "JetArea_Residual_EtaJES_GSC_Smear",
  "m_sort"                    : True,
  "m_redoJVT"                 : True,
  "m_inContainerName"         : "AntiKt4EMTopoJets",
  "m_outContainerName"        : "EMJets_Calibrated",
  "m_jetAlgo"                 : "AntiKt4EMTopo",
  "m_outputAlgo"              : "EMJets_Calibrated_Algo",
  "m_doCleaning"              : False,
  "m_jetCleanUgly"            : False,
  } )
# Select calibrated offline small-R EMTopo jets
c.algorithm("JetSelector", {
  "m_name"                    : "EMJetSelector",
  "m_msgLevel"                : msgLevel,
  "m_decorateSelectedObjects" : False,
  "m_cleanJets"               : True,
  "m_doJVT"                   : True,
  "m_noJVTVeto"               : False, # discard jets not passing JVT
  "m_haveTruthJets"           : False,
  "m_createSelectedContainer" : True,
  "m_inContainerName"         : "EMJets_Calibrated",
  "m_outContainerName"        : "EMJets_Selected",
  } )


# Calibrate L1 jets
c.algorithm("L1JetCalibrator", {
  "m_name"                  : "L1JetCalibrator",
  "m_msgLevel"              : msgLevel,
  "m_inContainerName"       : "jRoundJets",
  "m_outContainerName"      : "L1Jets_Calibrated",
  "m_L1MCJESROOTFileName"   : "TH2DResponse_vs_L1pt_eta_180920.root",
  "m_L1JetResponseHistName" : "AvgResponse_vs_L1pt_eta",
  "m_CorrVsPt"              : True,
  } )

# Save info to TTrees
c.algorithm("TreeAlgo",   {
  "m_name"                    : "outTree",
  "m_msgLevel"                : msgLevel,

  # trigger decisions
  "m_trigDetailStr"           : "basic passTriggers",

  # offline small-R jets
  "m_jetContainerName"        : "Jets_Selected EMJets_Selected",
  "m_jetBranchName"           : "jet EMjet",
  "m_jetDetailStr"            : "rapidity kinematic JVT",
  
  # HLT jets
  "m_trigJetContainerName"    : "HLT_xAOD__JetContainer_a4tcemsubjesISFS",
  "m_trigJetBranchName"       : "trigJet",
  "m_trigJetDetailStr"        : "rapidity kinematic",

  # jFEX jets
  "m_l1JetContainerName"      : "jRoundJets L1Jets_Calibrated LVL1JetRoIs",
  "m_l1JetBranchName"         : "jFEX jFEXcalibrated run2Jet",
  "m_sortL1Jets"              : True,
} )

