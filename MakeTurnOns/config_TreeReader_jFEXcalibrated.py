# PLAN: 
# Emulate several L1_jJ chains using calibrated and uncalibrated jFEX jets, examples:
# L1_jJ20  (reference HLT_j175)
# L1_jJ100 (reference HLT_j175)
# L1_4jJ15 (reference HLT_4j45)
# make turn-on curves as a function of HLT jet pt and offline jet pt

import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

msgLevel = "info"

SingleJetChains = { # list of alternative thresholds for emulating calibrated jet chains
  "L1_jJ15/"   : [29],
  "L1_jJ20/"   : [37],
  "L1_jJ50/"   : [79],
  "L1_jJ75/"   : [111],
  "L1_jJ100/"  : [143],
  "L1_jJ120/"  : [169],
}

MultiJetChains = {
  'L1_3jJ50/' : [79],
  'L1_4jJ15/' : [30],
  'L1_4jJ20/' : [37],
  'L1_5jJ15/' : [30],
  'L1_6jJ15/' : [29],
}

JetColls4Xaxis = [
  "AntiKt4EMPFlowJets",
  "HLT_xAOD__JetContainer_a4tcemsubjesISFS",
]

EfficiencyPoints = {
  "L1_jJ15/"  : '80',
  "L1_jJ20/"  : '100',
  "L1_jJ50/"  : '150',
  "L1_jJ75/"  : '200',
  "L1_jJ100/" : '250',
  "L1_jJ120/" : '300',
  'L1_3jJ50/' : '250',
  'L1_4jJ15/' : '175',
  'L1_4jJ20/' : '200',
  'L1_5jJ15/' : '130',
  'L1_6jJ15/' : '120',
}

###########################################################
## DO NOT MODIFY (below this line)
###########################################################

######################################################
# Produce turn-on curves with calibrated jFEX jets
######################################################

# Read TTrees
c.algorithm("TreeReader",   {
  "m_name"                : "TreeReaderCalibrated",
  "m_msgLevel"            : msgLevel,
  "m_jetBranchNames"      : "jet trigJet",
  "m_jetContainerNames"   : "AntiKt4EMPFlowJets HLT_xAOD__JetContainer_a4tcemsubjesISFS",
  "m_L1BranchNames"       : "jFEXcalibrated",
  "m_L1ContainerNames"    : "jRoundJets",
} )

# Emulation of single jet L1 chains
for key,thresholds in SingleJetChains.iteritems(): # loop over chains
  chain = key.split('/')[0]
  # Loop over ET thresholds
  for threshold in thresholds:
    # Loop over jet collections to use in x-axis
    for jetcoll in JetColls4Xaxis:
      # Turn-on curves vs pt
      c.algorithm("JetTriggerEfficiencies",   { 
        "m_name"                    : "EmulatejFEXcalibrated{}_vs_pt_{}".format(chain,jetcoll),
        "m_msgLevel"                : msgLevel,
        "m_fromNTUP"                : True,
        "m_jetTriggerMenuSet"       : "2018",
        "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
        "m_TDT"                     : False,
        "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
        "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
        "m_turnonString"            : key,
        "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
        "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turno  
        "m_overrideEtHLT"           : False,
        "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
        "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
        } )
      # Turn-on curves vs eta
      c.algorithm("JetTriggerEfficiencies",   { 
        "m_name"                    : "EmulatejFEXcalibrated{}_vs_eta_{}".format(chain,jetcoll),
        "m_msgLevel"                : msgLevel,
        "m_fromNTUP"                : True,
        "m_jetTriggerMenuSet"       : "2018",
        "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
        "m_TDT"                     : False,
        "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
        "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
        "m_turnonString"            : key,
        "m_selectionString"         : "auto+{'pt':"+EfficiencyPoints[key]+"}", # eg "auto | {'m': 45}"
        "m_variableString"          : "eta[0]", # The variable to plot as the x axis of the turno  
        "m_overrideEtHLT"           : False,
        "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
        "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
        } )

# Emulation of multiple jet L1 chains
for key,thresholds in MultiJetChains.iteritems(): # loop over chains
  chain = key.split('/')[0]
  # Loop over ET thresholds
  for threshold in thresholds:
    # Loop over jet collections to use in x-axis
    for jetcoll in JetColls4Xaxis:
      # Turn-on curves vs pt[mult-1]
      c.algorithm("JetTriggerEfficiencies",   { 
        "m_name"                    : "EmulateMultiJetjFEXcalibrated{}_vs_pt_nth_{}".format(chain,jetcoll),
        "m_msgLevel"                : msgLevel,
        "m_fromNTUP"                : True,
        "m_jetTriggerMenuSet"       : "2018",
        "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
        "m_TDT"                     : False,
        "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for L1 triggers and complicated HLT ones)
        "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
        "m_turnonString"            : key,
        "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
        "m_variableString"          : 'pt[{}]'.format(int(key.split('jJ')[0].split('_')[1])-1), # The variable to plot as the x axis of the turnon
        "m_overrideEtHLT"           : False,
        "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
        "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
        } )
      # Turn-on curves vs pt[mult]
      #c.algorithm("JetTriggerEfficiencies",   { 
      #  "m_name"                    : "EmulateMultiJetjFEXcalibrated{}_vs_pt_nplusth_{}".format(chain,jetcoll),
      #  "m_msgLevel"                : msgLevel,
      #  "m_fromNTUP"                : True,
      #  "m_jetTriggerMenuSet"       : "2018",
      #  "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
      #  "m_TDT"                     : False,
      #  "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for L1 triggers and complicated HLT ones)
      #  "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
      #  "m_turnonString"            : key,
      #  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
      #  "m_variableString"          : 'pt[{}]'.format(int(key.split('jJ')[0].split('_')[1])), # The variable to plot as the x axis of the turnon
      #  "m_overrideEtHLT"           : False,
      #  "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
      #  "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
      #  } )
      # Turn-on curves vs eta
      c.algorithm("JetTriggerEfficiencies",   { 
        "m_name"                    : "EmulateMultiJetjFEXcalibrated{}_vs_eta_nth_{}".format(chain,jetcoll),
        "m_msgLevel"                : msgLevel,
        "m_fromNTUP"                : True,
        "m_jetTriggerMenuSet"       : "2018",
        "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
        "m_TDT"                     : False,
        "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for L1 triggers and complicated HLT ones)
        "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
        "m_turnonString"            : key,
        "m_selectionString"         : "auto+{'pt':"+EfficiencyPoints[key]+"}", # eg "auto | {'m': 45}"
        "m_variableString"          : 'eta[{}]'.format(int(key.split('jJ')[0].split('_')[1])-1), # The variable to plot as the x axis of the turno vs multiplicity
        #"m_variableString"          : 'eta[{}]'.format(int(key.split('jJ')[0].split('_')[1])), # The variable to plot as the x axis of the turno  vs multiplicity +1
        "m_overrideEtHLT"           : False,
        "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
        "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
        } )
      # Turn-on curves vs eta
      #c.algorithm("JetTriggerEfficiencies",   { 
      #  "m_name"                    : "EmulateMultiJetjFEXcalibrated{}_vs_eta_nplusth_{}".format(chain,jetcoll),
      #  "m_msgLevel"                : msgLevel,
      #  "m_fromNTUP"                : True,
      #  "m_jetTriggerMenuSet"       : "2018",
      #  "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
      #  "m_TDT"                     : False,
      #  "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for L1 triggers and complicated HLT ones)
      #  "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
      #  "m_turnonString"            : key,
      #  "m_selectionString"         : "auto+{'pt':"+EfficiencyPoints[key]+"}", # eg "auto | {'m': 45}"
      #  "m_variableString"          : 'eta[{}]'.format(int(key.split('jJ')[0].split('_')[1])), # The variable to plot as the x axis of the turno  vs multiplicity +1
      #  "m_overrideEtHLT"           : False,
      #  "m_overrideTresholdL1"      : True,      # use alongside m_emulate to allow for overriding the threshold
      #  "m_TresholdL1"              : threshold, # use alongisde m_emulate and m_ThresholdHLT to specify the value to which we want to set the trigger threshold
      #  } )


