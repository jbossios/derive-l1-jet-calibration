import os,sys

# Example for testing NTUP production
sample = '/eos/atlas/atlascerngroupdisk/trig-jet/AOD_jFEXsamples/mc16_13TeV/AOD.21577847._000775.pool.root.1'
config = "config_NTUP_jFEX"

# TTree for producing efficiency plots
#sample = '/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/TTrees4TurnOns/HH4b/HH4b_450000_tree.root'
#config = "config_TreeReader_jFEX"
#config = "config_TreeReader_jFEXcalibrated"
#config = "config_TreeReader_jFEXcalibrated_scan"
#config = "config_TreeReader_Run2"

isNTUP = False

################################################
## DO NOT MODIFY
################################################

# choose appropiate config
CONFIG  = config
CONFIG += ".py"

# run xAH_run.py
command  = "python source/xAODAnaHelpers/scripts/xAH_run.py --config source/jetTriggerEfficiencies/data/"
command += CONFIG
command += " --files "
command += sample
if isNTUP:
  command += ' --treeName outTree/nominal'
command += " --force direct"
print(command)
os.system(command)
