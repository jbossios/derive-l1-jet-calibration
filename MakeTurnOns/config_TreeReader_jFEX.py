# PLAN: 
# Emulate several L1_jJ chains using calibrated and uncalibrated jFEX jets, examples:
# L1_jJ20  (reference HLT_j175)
# L1_jJ100 (reference HLT_j175)
# L1_4jJ15 (reference HLT_4j45)
# make turn-on curves as a function of HLT jet pt and offline jet pt

import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

msgLevel = "info"

SingleJetChains = [ # list of single jet chains
  "L1_jJ15/",
  "L1_jJ20/",
  "L1_jJ50/",
  "L1_jJ75/",
  "L1_jJ100/",
  "L1_jJ120/",
]

MultiJetChains = [ # list of multijet chains
  'L1_3jJ50/',
  'L1_4jJ15/',
  'L1_4jJ20/',
  'L1_5jJ15/',
  'L1_6jJ15/',
]

JetColls4Xaxis = [
  "AntiKt4EMPFlowJets",
  "HLT_xAOD__JetContainer_a4tcemsubjesISFS",
]

EfficiencyPoints = { # [mult-1] for single and [mult] for multijet
  "L1_jJ15/"  : '80', # 125
  "L1_jJ20/"  : '100', # 125
  "L1_jJ50/"  : '150', # 175
  "L1_jJ75/"  : '200', # 225
  "L1_jJ100/" : '250', # 275
  "L1_jJ120/" : '300', # 325
#  'L1_3jJ50/' : '175',
#  'L1_4jJ15/' : '125',
#  'L1_4jJ20/' : '125',
#  'L1_5jJ15/' : '125',
#  'L1_6jJ15/' : '150',
  'L1_3jJ50/' : '250',
  'L1_4jJ15/' : '175',
  'L1_4jJ20/' : '200',
  'L1_5jJ15/' : '130',
  'L1_6jJ15/' : '120',
}

###########################################################
## DO NOT MODIFY (below this line)
###########################################################

######################################################
# Produce turn-on curves with uncalibrated jFEX jets
######################################################

# Construct m_turnonString

# Single jet chains
m_SingleJetTurnonStr = ""
for key in SingleJetChains:
  if m_SingleJetTurnonStr != "":
    m_SingleJetTurnonStr += "|"
  m_SingleJetTurnonStr += key

# Selections for eta plots
selectionStringSingle = ""
for key in SingleJetChains:
  if selectionStringSingle != '': selectionStringSingle += '|'
  selectionStringSingle += "auto+{'pt':"+EfficiencyPoints[key]+"}"

# Multijet chains
m_MultiJetTurnonStr = ""
for key in MultiJetChains:
  if m_MultiJetTurnonStr != "":
    m_MultiJetTurnonStr += "|"
  m_MultiJetTurnonStr += key

# Selections for eta plots
selectionStringMulti = ""
for key in MultiJetChains:
  if selectionStringMulti != '': selectionStringMulti += '|'
  selectionStringMulti += "auto+{'pt':"+EfficiencyPoints[key]+"}"

# vs nth pt
variableString_vs_nth = ''
for key in MultiJetChains:
  if variableString_vs_nth != '': variableString_vs_nth += '|'
  variableString_vs_nth += 'pt[{}]'.format(int(key.split('jJ')[0].split('_')[1])-1)

# vs n+1th pt
variableString_vs_nplusth = ''
for key in MultiJetChains:
  if variableString_vs_nplusth != '': variableString_vs_nplusth += '|'
  variableString_vs_nplusth += 'pt[{}]'.format(int(key.split('jJ')[0].split('_')[1]))

# Read TTrees
c.algorithm("TreeReader",   {
  "m_name"                : "TreeReaderUncalibrated",
  "m_msgLevel"            : msgLevel,
  "m_jetBranchNames"      : "jet trigJet",
  "m_jetContainerNames"   : "AntiKt4EMPFlowJets HLT_xAOD__JetContainer_a4tcemsubjesISFS",
  "m_L1BranchNames"       : "jFEX",
  "m_L1ContainerNames"    : "jRoundJets",
} )

# Loop over jet collections to use in x-axis
for jetcoll in JetColls4Xaxis:
  # Emulation of single jet L1 chains
  c.algorithm("JetTriggerEfficiencies",   { 
    "m_name"                    : "EmulateSingleJetjFEXChains_vs_pt_{}".format(jetcoll),
    "m_msgLevel"                : msgLevel,
    "m_fromNTUP"                : True,
    "m_jetTriggerMenuSet"       : "2018",
    "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
    "m_TDT"                     : False,
    "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
    "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
    "m_turnonString"            : m_SingleJetTurnonStr,
    "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
    "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turno  
    } )
  
  # Now vs eta
  c.algorithm("JetTriggerEfficiencies",   { 
    "m_name"                    : "EmulateSingleJetjFEXChains_vs_eta_{}".format(jetcoll),
    "m_msgLevel"                : msgLevel,
    "m_fromNTUP"                : True,
    "m_jetTriggerMenuSet"       : "2018",
    "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
    "m_TDT"                     : False,
    "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
    "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
    "m_turnonString"            : m_SingleJetTurnonStr,
    "m_selectionString"         : selectionStringSingle, # eg "auto | {'m': 45}"
    "m_variableString"          : "eta[0]", # The variable to plot as the x axis of the turno  
    } )

  # Emulation of multiple jet L1 chains with small-R HLT jets as reference (vs nth)
  c.algorithm("JetTriggerEfficiencies",   { 
    "m_name"                    : "EmulateMultiJetjFEXChains_vs_pt_nth_{}".format(jetcoll),
    "m_msgLevel"                : msgLevel,
    "m_fromNTUP"                : True,
    "m_jetTriggerMenuSet"       : "2018",
    "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
    "m_TDT"                     : False,
    "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
    "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
    "m_turnonString"            : m_MultiJetTurnonStr,
    "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
    "m_variableString"          : variableString_vs_nth, # The variable to plot as the x axis of the turno  
    } )

  # Emulation of multiple jet L1 chains with small-R HLT jets as reference (vs n+1th)
  c.algorithm("JetTriggerEfficiencies",   { 
    "m_name"                    : "EmulateMultiJetjFEXChains_vs_pt_nplusth_{}".format(jetcoll),
    "m_msgLevel"                : msgLevel,
    "m_fromNTUP"                : True,
    "m_jetTriggerMenuSet"       : "2018",
    "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
    "m_TDT"                     : False,
    "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
    "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
    "m_turnonString"            : m_MultiJetTurnonStr,
    "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
    "m_variableString"          : variableString_vs_nplusth, # The variable to plot as the x axis of the turno  
    } )

  # Now vs eta (vs nth)
  #c.algorithm("JetTriggerEfficiencies",   { 
  #  "m_name"                    : "EmulateMultiJetjFEXChains_vs_eta_nth_{}".format(jetcoll),
  #  "m_msgLevel"                : msgLevel,
  #  "m_fromNTUP"                : True,
  #  "m_jetTriggerMenuSet"       : "2018",
  #  "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
  #  "m_TDT"                     : False,
  #  "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
  #  "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
  #  "m_turnonString"            : m_MultiJetTurnonStr,
  #  "m_selectionString"         : selectionStringMulti, # eg "auto | {'m': 45}"
  #  "m_variableString"          : variableString_vs_nth.replace('pt','eta'), # The variable to plot as the x axis of the turno  
  #  } )

  # Now vs eta (vs n+1th) (need to find efficiency points for n+1th case)
  c.algorithm("JetTriggerEfficiencies",   { 
    "m_name"                    : "EmulateMultiJetjFEXChains_vs_eta_nplusth_{}".format(jetcoll),
    "m_msgLevel"                : msgLevel,
    "m_fromNTUP"                : True,
    "m_jetTriggerMenuSet"       : "2018",
    "m_offlineContainerName"    : jetcoll, # the jet collection that will form the x axis of your turnons
    "m_TDT"                     : False,
    "m_emulate"                 : True, # emulate trigger decision from HLT jet collections (not implemented for complicated HLT ones)
    "m_requireTriggerInfoMatch" : False, # dontSeekMatch for get_trigger_info.py
    "m_turnonString"            : m_MultiJetTurnonStr,
    "m_selectionString"         : selectionStringMulti, # eg "auto | {'m': 45}"
    "m_variableString"          : variableString_vs_nplusth.replace('pt','eta'), # The variable to plot as the x axis of the turno  
    } )





