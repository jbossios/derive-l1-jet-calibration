# create folders

mkdir build source

# get xAH
cd source
git clone git@github.com:UCATLAS/xAODAnaHelpers.git

# get jetTriggerEfficiencies
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jetTriggerEfficiencies.git

# get L1JetCalibrator
git clone ssh://git@gitlab.cern.ch:7999/jbossios/l1jetcalibrator.git

# get jet-trigger-details
cd jetTriggerEfficiencies
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jet-trigger-details.git
cd jet-trigger-details
python get_trigger_info.py --menuSet 2018 --trigger HLT_j510 # dummy run to get a rulebook_hacked.py
cd ../../..

# move xAH config to proper location
cd source/jetTriggerEfficiencies
mkdir data
cd ../..
mv config_*.py source/jetTriggerEfficiencies/data/
